import groovy.json.JsonOutput

node {
  def distrobuilddir = "${pwd tmp: true}/uaqsbuild${env.BUILD_TAG}"
  def testrootdir = "${pwd()}/ua_quickstart_test${env.BUILD_TAG}"
  def hostdir = inhostvolume(env.UAQSTEST_HOSTHOME, env.JENKINS_HOME, testrootdir)
  def testbehatdir = "${pwd tmp: true}/ua_drupal_test${env.BUILD_TAG}"

  def webport = 5864
  def weburl = "http://web/"

  def behatjson = JsonOutput.toJson(
    [extensions:
      ['Behat\\\\MinkExtension': [base_url: weburl],
       'Drupal\\\\DrupalExtension': [drupal: [drupal_root: testrootdir],
                                   drush: [root: testrootdir]]]]
    )

  stage 'Setup'
  echo "'${behatjson}'"
  def uaqsbuilder = docker.build("uadrupal/builder", "${env.JENKINS_HOME}/phpplus")
  def uaqspackager = docker.build("uadrupal/packager", "${env.JENKINS_HOME}/phpplus")
  def uaqsinstaller = docker.build("uadrupal/installer", "${env.JENKINS_HOME}/phpplus")
  def uaqstester = docker.build("uadrupal/tester", "${env.JENKINS_HOME}/tester")
  def localweb = docker.build("uadrupal/localweb", "${env.JENKINS_HOME}/localweb")
  def database = docker.image('mysql')
  Closure indbcontext = {db ->
    uaqsinstaller.inside ("--link=${db.id}:database") {
      sh "sudo mkdir ${testrootdir} && sudo chown jenkins:jenkins ${testrootdir} && tar xzf ${distrobuilddir}/ua_quickstart-7.x-1.x-dev.tar.gz -C ${testrootdir} --strip-components=1"
      sh "cd ${testrootdir} && drush site-install ua_quickstart ua_quickstart_install_options_form.uaqs_verbosity=1 --account-mail=webmaster@uaquickstart.arizona.edu --account-name=userone --db-url=mysqli://uaqstestdbadmin:${env.UAQSTEST_DBPASS}@database:3306/uaqstestdb --site-name='UA Quickstart' -y"
    }
    Closure webcontext = {web ->
      uaqstester.inside("--link=${db.id}:database --link=${web.id}:web -e BEHAT_PARAMS='${behatjson}'") {
        sh "cd /testbehatdir ; bin/phantomjs --webdriver=4444 &"
        sh "cd /testbehatdir && bin/behat"
      }
    }
    def webcontainer = localweb.run("-p ${webport}:80 --restart=always --link=${db.id}:database --volume=${hostdir}:/var/www/html")
    try {
      webcontext.call(webcontainer)
    } finally {
      webcontainer.stop()
    }
  }

  stage 'Build'
  uaqsbuilder.inside {
    git url: 'https://bitbucket.org/ua_drupal/ua_quickstart.git', branch: '7.x-1.x'
    sh "sudo mkdir ${distrobuilddir} && sudo chown jenkins:jenkins ${distrobuilddir} && drush make --no-cache build-ua_quickstart.make ${distrobuilddir}/ua_quickstart"
  }

  stage 'Package'
  uaqspackager.inside {
    sh "cd ${distrobuilddir} && tar --exclude .gitignore -c -z -f ua_quickstart-7.x-1.x-dev.tar.gz ua_quickstart && zip -q -r ua_quickstart-7.x-1.x-dev.zip ua_quickstart -x\\*.gitignore && rm -Rf ua_quickstart"
  }
  deleteDir()

  stage 'Test'
  database.withRun ("--restart=always -e MYSQL_ROOT_PASSWORD=rottenradishrhizomes -e MYSQL_ONETIME_PASSWORD='true' -e MYSQL_DATABASE=uaqstestdb -e MYSQL_USER=uaqstestdbadmin -e MYSQL_PASSWORD=${env.UAQSTEST_DBPASS}", indbcontext)
  stage 'Archive'
  dir ("${distrobuilddir}") {
    archiveArtifacts artifacts: '*.tar.gz,*.zip', fingerprint: true
  }
}
@NonCPS
def inhostvolume(CharSequence hosthome, CharSequence ourhome, CharSequence path) {
  path.replaceFirst(/^${ourhome}/, hosthome)
}